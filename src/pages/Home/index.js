import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CardList from 'components/CardList';
import Select from '@material-ui/core/Select';
import styles from './Home.module.scss';
import { filterCategories, ALL_CATEGORIES } from 'utils/cards';
import { loadCards } from 'store/actions/cards';
import { connect } from 'react-redux';

class Home extends Component {
  static propTypes = {
    comunicationCards: PropTypes.object.isRequired,
    loadCards: PropTypes.func.isRequired,
    cards: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired
  };

  state = {
    chooseCategory: ALL_CATEGORIES
  };

  handleChangeCategory = ({ target: { value } }) => {
    this.setState({ chooseCategory: value });
  };

  componentDidMount() {
    const { loadCards, comunicationCards } = this.props;

    if (
      !comunicationCards.isLoad &&
      !comunicationCards.isLoading &&
      !comunicationCards.isErrorLoad
    ) {
      loadCards();
      return;
    }
  }
  render() {
    const { cards, categories } = this.props;
    const { chooseCategory } = this.state;
    return (
      <div className={styles.container}>
        <div className={styles.containerSelect}>
          <Select
            native
            value={chooseCategory}
            onChange={this.handleChangeCategory}
            inputProps={{
              name: 'age',
              id: 'age-native-simple'
            }}
          >
            <option value={ALL_CATEGORIES}>{ALL_CATEGORIES}</option>
            {categories &&
              categories.map(category => {
                return (
                  <option key={category} value={category}>
                    {category}
                  </option>
                );
              })}
          </Select>
        </div>
        <CardList cards={filterCategories(cards, chooseCategory)} />
      </div>
    );
  }
}

const mapStateToProps = ({ communication, cards }) => {
  return {
    comunicationCards: communication.cards,
    cards: cards.data,
    categories: cards.category
  };
};
const mapDispatchToProps = { loadCards };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
