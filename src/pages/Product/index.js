import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Tools from 'components/Tools';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import KeyboardReturn from '@material-ui/icons/KeyboardReturn';
import SwipeableViews from 'react-swipeable-views';
import { Link } from 'react-router-dom';
import { openCard, loadCard } from 'store/actions/cards';
import { connect } from 'react-redux';
import styles from './Product.module.scss';

class ProductPage extends Component {
  static propTypes = {
    card: PropTypes.object,
    id: PropTypes.string.isRequired,
    openProduct: PropTypes.string.isRequired,
    openCard: PropTypes.func.isRequired,
    comunicationOpenCard: PropTypes.object,
    loadCard: PropTypes.func.isRequired
  };

  state = {
    valueTab: 0
  };

  handleChangeTabs = (event, value) => {
    this.setState({ valueTab: value });
  };

  loadDataCard = () => {
    const { id, comunicationOpenCard, loadCard } = this.props;

    if (
      !comunicationOpenCard.isLoad &&
      !comunicationOpenCard.isLoading &&
      !comunicationOpenCard.isErrorLoad
    ) {
      loadCard(id);
    }
  };

  componentDidUpdate() {
    this.loadDataCard();
  }

  componentDidMount() {
    const { openCard, openProduct, id } = this.props;

    if (!openProduct) {
      return openCard(id);
    }
    this.loadDataCard();
  }
  render() {
    const { card, id, comunicationOpenCard } = this.props;
    const { valueTab } = this.state;

    if (!comunicationOpenCard || !comunicationOpenCard.isLoad) return <div />;

    const { title, img, description, specifications, price } = card;
    return (
      <div className={styles.container}>
        <Grid container direction="row" justify="center">
          <Grid
            item
            className={styles.body}
            xs={12}
            sm={12}
            md={11}
            lg={10}
            xl={9}
          >
            <Link to={'/'}>
              <Button className={styles.btn}>
                <KeyboardReturn />
                Back
              </Button>
            </Link>
            <h1 className={styles.header}>{title}</h1>
            <div className={styles.containerImg}>
              <img className={styles.img} src={img} alt={title} />
            </div>

            <Tools card={card} id={id} price={price} />
            <Paper square>
              <Tabs
                className={styles.tabs}
                value={valueTab}
                onChange={this.handleChangeTabs}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab label="Description" />
                <Tab label="Specifications" />
              </Tabs>
            </Paper>
            <div className={styles.content}>
              <SwipeableViews axis={'x-reverse'} index={valueTab}>
                {(valueTab === 0 && <p>{description}</p>) || <div />}
                {(valueTab === 1 && <p>{specifications}</p>) || <div />}
              </SwipeableViews>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({ cards, communication }, { id }) => {
  return {
    card: cards.dataCard[id],
    openProduct: cards.openCard,
    comunicationOpenCard: communication.loadCards[id]
  };
};

const mapDispatchToProps = { openCard, loadCard };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductPage);
