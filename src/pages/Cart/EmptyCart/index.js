import React from 'react';
import styles from './EmptyCart.module.scss';

const EmptyCart = () => {
  return <div className={styles.container}>No products in the cart.</div>;
};

export default EmptyCart;
