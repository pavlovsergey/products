import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import EmptyCart from './EmptyCart';
import GeneralTotal from 'components/GeneralTotal';
import CartList from 'components/CartList';
import { connect } from 'react-redux';
import styles from './Cart.module.scss';

class Cart extends Component {
  static propTypes = {
    cart: PropTypes.object.isRequired
  };
  getSummary = () => {
    const { cart } = this.props;
    const summary = Object.values(cart).reduce((acc, item) => {
      acc += item.price * item.quantity;
      return acc;
    }, 0);

    return summary;
  };
  render() {
    const { cart } = this.props;

    if (Object.keys(cart).length === 0) {
      return <EmptyCart />;
    }

    return (
      <div className={styles.container}>
        <Grid container direction="row" justify="center">
          <Grid
            item
            className={styles.body}
            xs={12}
            sm={12}
            md={11}
            lg={10}
            xl={9}
          >
            <CartList cart={cart} />
            <GeneralTotal />
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({ cart }) => {
  return {
    cart: cart.data
  };
};

export default connect(mapStateToProps)(Cart);
