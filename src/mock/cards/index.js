export const cards = [
  {
    id: '1',
    title: 'ASUS VIVOBOOK E12 E203NA ',
    img:
      'http://www.dkt.ua/pub/media/catalog/category/hp-envy-x360-15-aq173cl-2.jpg',
    category: 'best laptops'
  },
  {
    id: '2',
    title: 'LENOVO IDEAPAD 330-15IKB',
    img:
      'https://www.dkt.ua/pub/media/catalog/product/cache/5ee9104871b9922e2fe601739024efaa/l/e/lenovo-ideapad-330-15-black-1.jpg',
    category: 'good laptops'
  },
  {
    id: '3',
    title: 'Dell G3 3579 (G35581S1NDL-60B) Blac',
    img:
      'http://mta.ua/image/data/stat/filter/gaming-notebook/gaming-notebook.jpg',
    category: 'good laptops'
  },
  {
    id: '4',
    title: 'ASUS VIVOBOOK X540UA',
    img:
      'https://www.dkt.ua/pub/media/catalog/product/cache/5ee9104871b9922e2fe601739024efaa/a/s/asus-vivobook-x540ua-black-1.jpg',
    category: 'best laptops'
  },
  {
    id: '5',
    title: 'Ноутбук Dell Inspiron 15 35',
    img:
      ' http://www.dkt.ua/pub/media/catalog/product/cache/5ee9104871b9922e2fe601739024efaa/d/e/dell-inspiron-15-3567-black-1.jpg',
    category: 'best laptops'
  }
];
