export const card = {
  '1': {
    id: '1',
    price: 200,
    title: 'ASUS VIVOBOOK E12 E203NA ',
    img:
      'http://www.dkt.ua/pub/media/catalog/category/hp-envy-x360-15-aq173cl-2.jpg',
    description:
      'Для работы и развлечений – каждый день!  Asus X560 – это современный ноутбук, объединяющий в себе красивый дизайн с современными технологиями. Он предлагает пользователю всю мощь новейшего процессора в сочетании с видеокартой игрового класса. Для вывода изображения у него имеется тонкорамочный дисплей NanoEdge.',
    specifications:
      'INTEL CORE I3-8130U (2.2ГГЦ-3.4ГГЦ, 4МБ CACHE), WINDOWS 10 HOME, 4 ГБ / 1 ТБ, 15.6” (1366X768) МАТОВЫЙ ANTIGLARE HD, INTEL UHD GRAPHICS 620, LAN 10/100/1000, BLUETOOTH 4.1, WIFI 802.11AC, SD/SDHC/SDXC/MMC, 1XUSB 3.1 TYPE-C, 2XUSB 3.0, HDMI, ВЕБ-КАМЕРА, ЦВЕТ CORAL RED (КРАСНЫЙ)'
  },
  '2': {
    id: '2',
    price: 1200,
    title: 'LENOVO IDEAPAD 330-15IKB',
    img:
      'https://www.dkt.ua/pub/media/catalog/product/cache/5ee9104871b9922e2fe601739024efaa/l/e/lenovo-ideapad-330-15-black-1.jpg',
    description:
      'Стильный корпус с контрастными акцентамиЧерная отделка корпуса дополнена контрастными голубыми акцентами на гранях – ноутбук X560 выделяется среди других моделей своим стильным внешним видом.',
    specifications:
      'INTEL CORE I3-8130U (2.2ГГЦ-3.4ГГЦ, 4МБ CACHE), WINDOWS 10 HOME, 4 ГБ / 1 ТБ, 15.6” (1366X768) МАТОВЫЙ ANTIGLARE HD, INTEL UHD GRAPHICS 620, LAN 10/100/1000, BLUETOOTH 4.1, WIFI 802.11AC, SD/SDHC/SDXC/MMC, 1XUSB 3.1 TYPE-C, 2XUSB 3.0, HDMI, ВЕБ-КАМЕРА, ЦВЕТ CORAL RED (КРАСНЫЙ)'
  },
  '3': {
    id: '3',
    price: 2200,
    title: 'Dell G3 3579 (G35581S1NDL-60B) Blac',
    img:
      'http://mta.ua/image/data/stat/filter/gaming-notebook/gaming-notebook.jpg',
    description:
      'Современная конфигурация  Ноутбук X560 обладает достаточно мощной конфигурацией, чтобы обеспечить эффективную работу в многозадачном режиме, а его дисплей выдает безукоризненное изображение в развлекательных приложениях. Для высокоскоростного подключения к интернету имеется беспроводной модуль Wi-Fi стандарта 802.11ac.',
    specifications:
      'INTEL CORE I3-8130U (2.2ГГЦ-3.4ГГЦ, 4МБ CACHE), WINDOWS 10 HOME, 4 ГБ / 1 ТБ, 15.6” (1366X768) МАТОВЫЙ ANTIGLARE HD, INTEL UHD GRAPHICS 620, LAN 10/100/1000, BLUETOOTH 4.1, WIFI 802.11AC, SD/SDHC/SDXC/MMC, 1XUSB 3.1 TYPE-C, 2XUSB 3.0, HDMI, ВЕБ-КАМЕРА, ЦВЕТ CORAL RED (КРАСНЫЙ)'
  },
  '4': {
    id: '4',
    price: 2040,
    title: 'ASUS VIVOBOOK X540UA',
    img:
      'https://www.dkt.ua/pub/media/catalog/product/cache/5ee9104871b9922e2fe601739024efaa/a/s/asus-vivobook-x540ua-black-1.jpg',
    description:
      'Современная конфигурация  Ноутбук X560 обладает достаточно мощной конфигурацией, чтобы обеспечить эффективную работу в многозадачном режиме, а его дисплей выдает безукоризненное изображение в развлекательных приложениях. Для высокоскоростного подключения к интернету имеется беспроводной модуль Wi-Fi стандарта 802.11ac.',
    specifications:
      'INTEL CORE I3-8130U (2.2ГГЦ-3.4ГГЦ, 4МБ CACHE), WINDOWS 10 HOME, 4 ГБ / 1 ТБ, 15.6” (1366X768) МАТОВЫЙ ANTIGLARE HD, INTEL UHD GRAPHICS 620, LAN 10/100/1000, BLUETOOTH 4.1, WIFI 802.11AC, SD/SDHC/SDXC/MMC, 1XUSB 3.1 TYPE-C, 2XUSB 3.0, HDMI, ВЕБ-КАМЕРА, ЦВЕТ CORAL RED (КРАСНЫЙ)'
  },
  '5': {
    id: '5',
    price: 2010,
    title: 'Ноутбук Dell Inspiron 15 35',
    img:
      ' http://www.dkt.ua/pub/media/catalog/product/cache/5ee9104871b9922e2fe601739024efaa/d/e/dell-inspiron-15-3567-black-1.jpg',
    description:
      'Для работы и развлечений – каждый день!  Asus X560 – это современный ноутбук, объединяющий в себе красивый дизайн с современными технологиями. Он предлагает пользователю всю мощь новейшего процессора в сочетании с видеокартой игрового класса. Для вывода изображения у него имеется тонкорамочный дисплей NanoEdge.',
    specifications:
      'INTEL CORE I3-8130U (2.2ГГЦ-3.4ГГЦ, 4МБ CACHE), WINDOWS 10 HOME, 4 ГБ / 1 ТБ, 15.6” (1366X768) МАТОВЫЙ ANTIGLARE HD, INTEL UHD GRAPHICS 620, LAN 10/100/1000, BLUETOOTH 4.1, WIFI 802.11AC, SD/SDHC/SDXC/MMC, 1XUSB 3.1 TYPE-C, 2XUSB 3.0, HDMI, ВЕБ-КАМЕРА, ЦВЕТ CORAL RED (КРАСНЫЙ)'
  }
};
