import React, { Component } from 'react';
import styles from './Layout.module.scss';
import HeaderBar from 'components/HeaderBar';
import Routes from 'routes';
import Footer from 'components/Footer';

class Layout extends Component {
  render() {
    return (
      <div className={styles.container}>
        <HeaderBar />

        <Routes />

        <Footer />
      </div>
    );
  }
}

export default Layout;
