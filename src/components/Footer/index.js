import React from 'react';
import styles from './Footer.module.scss';

const Footer = () => (
  <div className={styles.container}>
    <div className={styles.field}> &copy;2019 Pavlov Sergey</div>
  </div>
);

export default Footer;
