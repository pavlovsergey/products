import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Navigator from './Navigator';
import GeneralTotal from 'components/GeneralTotal';
import styles from './HeaderBar.module.scss';

const HeaderBar = () => (
  <div className={styles.container}>
    <AppBar position="static">
      <Toolbar className={styles.body}>
        <h1 className={styles.brand}>
          <span className={styles.firstPart}>Product</span>
          <span className={styles.secondPart}>Catalog</span>
        </h1>
        <Navigator />
      </Toolbar>
    </AppBar>
    <GeneralTotal />
  </div>
);

export default HeaderBar;
