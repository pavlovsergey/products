import React, { Component } from 'react';
import PropTypes from 'prop-types';
import history from 'routes/history';
import { getLastStringOfHref } from 'utils/location';
import { routes } from 'routes/constants';
import { connect } from 'react-redux';
import styles from './Navigator.module.scss';

class Navigator extends Component {
  static propTypes = {
    page: PropTypes.number
  };
  state = {
    active: getLastStringOfHref('/')
  };

  handleClickLink = path => () => {
    this.setState({ active: path }, history.push(path));
  };

  componentDidUpdate() {
    const { active } = this.state;
    const route = getLastStringOfHref('/');
    if (route !== active) this.setState({ active: route });
  }

  render() {
    const { active } = this.state;
    return (
      <div className={styles.container}>
        <div className={styles.navigation}>
          {routes.map((item, index) => (
            <div
              key={index}
              onClick={this.handleClickLink(item.path[0])}
              className={
                item.path.indexOf(active) === -1
                  ? styles.item
                  : [styles.item, styles.active].join(' ')
              }
            >
              {item.text}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ cards }) => {
  return {
    openCard: cards.openCard
  };
};

export default connect(mapStateToProps)(Navigator);
