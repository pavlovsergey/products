import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './GeneralTotal.module.scss';

class GeneralTotal extends Component {
  static propTypes = {
    cart: PropTypes.object.isRequired
  };
  getSummary = () => {
    const { cart } = this.props;
    const summary = Object.values(cart).reduce((acc, item) => {
      acc += item.price * item.quantity;
      return acc;
    }, 0);

    return summary;
  };
  render() {
    return (
      <div className={styles.summary}>
        <div className={styles.summaryTextHeader}>To pay: </div>
        <div className={styles.summaryText}>{this.getSummary()}$ </div>
      </div>
    );
  }
}

const mapStateToProps = ({ cart }) => {
  return {
    cart: cart.data
  };
};

export default connect(mapStateToProps)(GeneralTotal);
