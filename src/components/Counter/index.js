import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Counter.module.scss';

class Counter extends Component {
  static propTypes = {
    count: PropTypes.number.isRequired,
    plus: PropTypes.func.isRequired,
    minus: PropTypes.func.isRequired
  };

  render() {
    const { count, plus, minus } = this.props;

    return (
      <div className={styles.container}>
        <div onClick={plus} className={styles.block}>
          <span className={styles.symbol}>+</span>
        </div>
        <div className={styles.count}>{count}</div>
        <div onClick={minus} className={styles.block}>
          <span className={styles.symbol}>-</span>
        </div>
      </div>
    );
  }
}

export default Counter;
