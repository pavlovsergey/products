import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Hidden from '@material-ui/core/Hidden';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import DeleteForever from '@material-ui/icons/DeleteForever';
import Counter from 'components/Counter';
import { setOrderQuantity, deleteOrder } from 'store/actions/cart';
import { connect } from 'react-redux';
import styles from './OrderItem.module.scss';

class OrderItem extends Component {
  static propTypes = {
    img: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    setOrderQuantity: PropTypes.func.isRequired,
    deleteOrder: PropTypes.func.isRequired
  };

  handleAddCount = () => {
    const { setOrderQuantity, quantity, id } = this.props;
    setOrderQuantity(quantity + 1, id);
  };

  handleReduceCount = () => {
    const { setOrderQuantity, quantity, id } = this.props;
    if (quantity !== 1) setOrderQuantity(quantity - 1, id);
  };

  handleDeleteOrder = () => {
    const { deleteOrder, id } = this.props;
    deleteOrder(id);
  };
  render() {
    const { img, title, quantity, price } = this.props;
    return (
      <TableRow>
        <Hidden mdDown>
          <TableCell>
            <img className={styles.img} src={img} alt="card" />
          </TableCell>
        </Hidden>
        <TableCell> {title} </TableCell>
        <Hidden smDown>
          <TableCell>
            <Counter
              plus={this.handleAddCount}
              minus={this.handleReduceCount}
              count={quantity}
            />
          </TableCell>
        </Hidden>
        <Hidden mdDown>
          <TableCell> {price}$ </TableCell>
        </Hidden>
        <TableCell> {price * quantity}$ </TableCell>
        <TableCell>
          <DeleteForever
            onClick={this.handleDeleteOrder}
            className={styles.btn}
          />
        </TableCell>
      </TableRow>
    );
  }
}

const mapDispatchToProps = { setOrderQuantity, deleteOrder };

export default connect(
  null,
  mapDispatchToProps
)(OrderItem);
