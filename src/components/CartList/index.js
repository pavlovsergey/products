import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import Hidden from '@material-ui/core/Hidden';
import OrderItem from './OrderItem';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';

class CartList extends Component {
  static propTypes = {
    cart: PropTypes.object.isRequired
  };

  render() {
    const { cart } = this.props;
    return (
      <Table>
        <TableHead>
          <TableRow>
            <Hidden mdDown>
              <TableCell> Image </TableCell>
            </Hidden>
            <TableCell>Name</TableCell>
            <Hidden smDown>
              <TableCell>Quantity</TableCell>
            </Hidden>
            <Hidden mdDown>
              <TableCell>Price</TableCell>
            </Hidden>
            <TableCell>Total</TableCell>
            <TableCell>Delete</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Object.values(cart).map(order => (
            <OrderItem key={order.id} {...order} />
          ))}
        </TableBody>
      </Table>
    );
  }
}

export default CartList;
