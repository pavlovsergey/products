import React, { Component } from 'react';
import ProductCard from './ProductCard';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import styles from './CardList.module.scss';

class CardList extends Component {
  static propTypes = {
    cards: PropTypes.array.isRequired
  };

  render() {
    const { cards } = this.props;
    return (
      <Grid container spacing={4}>
        {cards.map(card => (
          <Grid key={card.id} item xs={12} sm={12} md={6} lg={3} xl={2}>
            <div className={styles.container}>
              <ProductCard card={card} />
            </div>
          </Grid>
        ))}
      </Grid>
    );
  }
}

export default CardList;
