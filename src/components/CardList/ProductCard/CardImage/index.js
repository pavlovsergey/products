import React from 'react';
import PropTypes from 'prop-types';

import styles from './CardImage.module.scss';

const CardImage = ({ image }) => {
  return (
    <div className={styles.container}>
      <img className={styles.img} src={image} alt="card" />
    </div>
  );
};
CardImage.propTypes = {
  images: PropTypes.string
};

export default CardImage;
