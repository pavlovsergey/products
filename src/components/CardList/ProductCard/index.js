import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardImage from './CardImage';
import history from 'routes/history';
import { openCard } from 'store/actions/cards';
import { connect } from 'react-redux';
import styles from './ProductCard.module.scss';

class ProductCard extends Component {
  static propTypes = {
    card: PropTypes.object.isRequired,
    openCard: PropTypes.func.isRequired
  };

  handleRedirect = () => {
    const {
      openCard,
      card: { id }
    } = this.props;
    openCard(id);
    history.push(`/product/${id}`);
  };
  render() {
    const {
      card: { title, img }
    } = this.props;

    return (
      <Card className={styles.card}>
        <CardActionArea onClick={this.handleRedirect}>
          <CardImage image={img} />
        </CardActionArea>

        <div className={styles.title}>{title}</div>
      </Card>
    );
  }
}

const mapDispatchToProps = { openCard };

export default connect(
  null,
  mapDispatchToProps
)(ProductCard);
