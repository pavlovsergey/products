import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Counter from 'components/Counter';
import { addToCart } from 'store/actions/cart';
import { connect } from 'react-redux';
import styles from './Tools.module.scss';

class Tools extends Component {
  static propTypes = {
    card: PropTypes.object.isRequired,
    cart: PropTypes.object.isRequired,
    price: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired
  };

  state = {
    count: 1
  };
  handleAddCount = () => {
    const { count } = this.state;
    this.setState({ count: count + 1 });
  };
  handleAddToCart = () => {
    const { card, id, addToCart } = this.props;
    const { count } = this.state;
    addToCart({ ...card, quantity: count }, id);
  };
  handleReduceCount = () => {
    const { count } = this.state;
    if (count !== 1) this.setState({ count: count - 1 });
  };
  render() {
    const { price, cart, id } = this.props;
    const { count } = this.state;

    return (
      <div className={styles.container}>
        <div className={styles.block}>
          <span className={styles.header}>Price: </span>
          <span className={styles.value}>{price}$ </span>
        </div>
        <div className={styles.block}>
          <span className={styles.header}>Count: </span>
          <Counter
            plus={this.handleAddCount}
            minus={this.handleReduceCount}
            count={count}
          />
        </div>

        <div className={styles.block}>
          <span className={styles.header}>Total: </span>
          <span className={styles.value}>{price * count}$ </span>
        </div>
        {!cart[id] && (
          <div onClick={this.handleAddToCart} className={styles.btn}>
            Add to cart
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ cart }) => {
  return {
    cart: cart.data
  };
};

const mapDispatchToProps = { addToCart };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tools);
