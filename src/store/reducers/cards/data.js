import { LOAD_CARDS } from 'store/constants/communication';

const stateCardDefault = [];

const data = (state = stateCardDefault, { type, payload }) => {
  switch (type) {
    case LOAD_CARDS:
      return payload.data;

    default:
      return state;
  }
};

export default data;
