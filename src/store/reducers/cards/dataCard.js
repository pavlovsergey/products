import { LOAD_CARD } from 'store/constants/communication';

const stateCardDefault = {};

const dataCard = (state = stateCardDefault, { type, payload }) => {
  switch (type) {
    case LOAD_CARD:
      return {
        ...state,
        [payload.id]: payload.data
      };
    default:
      return state;
  }
};

export default dataCard;
