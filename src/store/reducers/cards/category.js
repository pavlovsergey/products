import { LOAD_CARDS } from 'store/constants/communication';
import { getCategories } from 'utils/cards';

const stateDefault = [];

const category = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case LOAD_CARDS:
      return getCategories(payload.data);

    default:
      return state;
  }
};

export default category;
