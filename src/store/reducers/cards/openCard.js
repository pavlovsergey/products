import { OPEN_CARD } from 'store/constants/communication';
const stateDefault = '';

const openCard = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case OPEN_CARD:
      return payload.id.toString();

    default:
      return state;
  }
};

export default openCard;
