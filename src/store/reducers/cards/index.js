import { combineReducers } from 'redux';
import data from './data';
import openCard from './openCard';
import dataCard from './dataCard';
import category from './category';

export default combineReducers({ data, openCard, dataCard, category });
