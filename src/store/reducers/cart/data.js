import {
  ADD_ORDER,
  SET_ORDER_QUANTITY,
  DELETE_ORDER
} from 'store/constants/cart';

const stateCardDefault = {};

const dataCard = (state = stateCardDefault, { type, payload }) => {
  switch (type) {
    case ADD_ORDER:
      return {
        ...state,
        [payload.id]: payload.order
      };

    case SET_ORDER_QUANTITY:
      return {
        ...state,
        [payload.id]: {
          ...state[payload.id],
          quantity: payload.quantity
        }
      };

    case DELETE_ORDER:
      delete state[payload.id];
      return { ...state };

    default:
      return state;
  }
};

export default dataCard;
