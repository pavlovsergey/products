import { combineReducers } from 'redux';
import cards from './cards';
import loadCards from './loadCards';

export default combineReducers({
  cards,
  loadCards
});
