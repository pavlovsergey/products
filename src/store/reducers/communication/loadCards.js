import { LOAD_CARD, OPEN_CARD } from 'store/constants/communication';
import { loadingAction, errAction } from 'utils/store/communication';
import {
  setLoadingTrue,
  setLoadTrue,
  setLoadFail
} from 'utils/store/communication';

const defaultState = {
  isLoading: false,
  isLoad: false,
  isErrorLoad: false,
  error: { message: '', name: '' }
};

const loadCards = (state = {}, { type, payload }) => {
  switch (type) {
    case OPEN_CARD:
      if (state[payload.id]) return state;
      return {
        ...state,
        [payload.id]: defaultState
      };

    case loadingAction(LOAD_CARD):
      return {
        ...state,
        [payload.id]: setLoadingTrue(state[payload.id])
      };

    case LOAD_CARD:
      return {
        ...state,
        [payload.id]: setLoadTrue(state[payload.id])
      };

    case errAction(LOAD_CARD):
      return {
        ...state,
        [payload.id]: setLoadFail(state[payload.id], payload)
      };

    default:
      return state;
  }
};
export default loadCards;
