import { combineReducers } from 'redux';
import communication from './communication';
import cards from './cards';
import cart from './cart';

export default combineReducers({
  communication,
  cards,
  cart
});
