export const handlerError = (type, message, e, props) => {
  return {
    type,
    payload: {
      message: `${message} ${(e && e.message) || ''}`,
      ...props
    }
  };
};
