import {
  LOAD_CARDS,
  LOAD_CARD,
  OPEN_CARD
} from 'store/constants/communication';
import { handlerError } from './error';
import { loadingAction, errAction } from 'utils/store/communication';
import { cards } from 'mock/cards';
import { card } from 'mock/card';

export const loadCards = () => {
  return async dispatch => {
    try {
      dispatch({ type: loadingAction(LOAD_CARDS) });

      return dispatch({
        type: LOAD_CARDS,
        payload: { data: cards }
      });
    } catch (e) {
      const message = '';
      dispatch(handlerError(errAction(LOAD_CARDS), message, e));
    }
  };
};

export const openCard = id => {
  return {
    type: OPEN_CARD,
    payload: { id }
  };
};
export const loadCard = id => {
  return async dispatch => {
    try {
      dispatch({ type: loadingAction(LOAD_CARD), payload: { id } });

      return dispatch({
        type: LOAD_CARD,
        payload: { data: card[id], id }
      });
    } catch (e) {
      const message = '';
      dispatch(handlerError(errAction(LOAD_CARD), message, e, { id }));
    }
  };
};
