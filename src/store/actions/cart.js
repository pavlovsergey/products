import {
  ADD_ORDER,
  SET_ORDER_QUANTITY,
  DELETE_ORDER
} from 'store/constants/cart';
export const addToCart = (order, id) => {
  return {
    type: ADD_ORDER,
    payload: {
      order,
      id
    }
  };
};

export const setOrderQuantity = (quantity, id) => {
  return {
    type: SET_ORDER_QUANTITY,
    payload: {
      quantity,
      id
    }
  };
};

export const deleteOrder = id => {
  return {
    type: DELETE_ORDER,
    payload: { id }
  };
};
