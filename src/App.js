import React from 'react';
import Layout from './Layout';
import { Provider } from 'react-redux';
import store from './store';
import { Router } from 'react-router-dom';
import history from './routes/history';

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Layout />
      </Router>
    </Provider>
  );
};

export default App;
