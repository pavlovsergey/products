export const ALL_CATEGORIES = 'All categories';

export const getCategories = data => {
  const category = [];
  data.forEach(item => {
    if (category.indexOf(item.category) === -1) {
      category.push(item.category);
    }
    return;
  });
  return category;
};

export const filterCategories = (data, chooseCategory) => {
  if (ALL_CATEGORIES === chooseCategory) return data;
  return data.filter(item => {
    if (item.category === chooseCategory) {
      return true;
    }
    return false;
  });
};
