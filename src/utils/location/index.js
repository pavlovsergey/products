export const getLastStringOfHref = subString => {
  const href = window.location.href;
  const targetIndex = href.lastIndexOf(subString);
  return href.substr(targetIndex);
};
