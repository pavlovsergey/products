import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from 'pages/Home';
import Product from 'pages/Product';
import Cart from 'pages/Cart';

class Routes extends Component {
  getProductPage = ({ match }) => {
    return <Product id={match.params.id} />;
  };
  render() {
    return (
      <Switch>
        <Route path="/product/:id" exact children={this.getProductPage} />
        <Route path="/cart" component={Cart} />
        <Route path="/" component={Home} />
      </Switch>
    );
  }
}
export default Routes;
